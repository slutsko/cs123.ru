<?php

/** @var yii\web\View $this */

use app\components\CategoryWidget;
use app\components\FeedbackNameWidget;
use app\components\FeedbackTextWidget;
use app\components\TarifItemWidget;
use app\components\StageItemWidget;
use app\components\ComponentItemWidget;
use app\components\BannerWidget;
use app\components\EmailFormWidget;

$this->title = 'Бухгалтерский Экспресс Венеры Юматовой';
?>
<!-- banner-section -->
<section class="banner-section style-three">
    <a name='start'></a>
    <div class="banner-carousel owl-theme owl-carousel owl-dots-none">
        <?php
            echo BannerWidget::widget([
                'title' => 'Бухгалтерские услуги в Омске',
                'text' => 'Когда в ежедневной работе нужен кто-то больше, чем просто бухгалтер',
                'url' => '#services',
                'urlTitle' => 'Наши услуги',
                'image' => 'images/banner/6.jpg',
            ]);
            echo BannerWidget::widget([
                'title' => 'Наши клиенты такие разные',
                'text' => 'Но мы все равно любим всех одинаково',
                'url' => '#clients',
                'urlTitle' => 'Наши клиенты',
                'image' => 'images/banner/2.jpg',
            ]);
            echo BannerWidget::widget([
                'title' => 'Мы делаем почти все',
                'text' => 'и цены у нас отличные',
                'url' => '#tarifs',
                'urlTitle' => 'Наши тарифы',
                'image' => 'images/banner/3.jpg',
            ]);
            echo BannerWidget::widget([
                'title' => 'Нас легко найти. Мы везде',
                'text' => '
                    <ul class="info-list-banner clearfix">
                        <li><i class="fas fa-map-marker-alt"></i> г. Омск, ул. Щербанева, д. 25, оф. 404.3 </li>
                        <li><i class="fas fa-envelope"></i>Email <a href="mailto:office@cs123.ru"> office@cs123.ru</a></li>
                        <li><i class="fas fa-headphones"></i><a href="tel:+79620353468"> +7 (962)  035-34-68</a></li>
                    </ul>
                    <ul class="social-links-banner clearfix">
                        <li><a href="https://www.facebook.com/venera.yumatova"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://www.instagram.com/buhgalter.konsultant/"><i class="fab fa-instagram"></i></a>
                        <a href="https://vk.ru/buhgalter.konsultant"><i class="fab fa-vk"></i></a></li>
                        </ul>
                ',
                'url' => '#contact',
                'urlTitle' => 'Где мы?',
                'image' => 'images/banner/4.jpg',
            ]);
        ?>
    </div>
</section>
<!-- banner-section end -->


<!-- intro-section -->
<section class="intro-section">
    <a name='about'></a>
    <div class="auto-container">
        <div class="upper-content">
            <div class="row clearfix">
                <div class="single-column">
                    <div class="single-item">
                        <div class="inner-box">
                            <p>Здравствуйте!<br />
                                Меня зовут Юматова Венера - я эксперт по бухгалтерскому учету и аудиту, а также руководитель компании.<br />
                                В моей команде работают только профессионалы, а я тщательно контролирую их работу и несу полную ответственность за результат перед клиентами.<br />
                                При работе с нами, вы получите:</p>
                        </div>
                    </div>
                </div>
                <?php
                echo ComponentItemWidget::widget([
                    'text' => 'Бухгалтера и аудитора в одном лице',
                    'icon' => 'flaticon-woman',
                ]);
                echo ComponentItemWidget::widget([
                    'image' => 'images/main.jpg',
                ]);
                echo ComponentItemWidget::widget([
                    'text' => 'Какая бы задача у вас ни возникла, мы ее уже решали и легко сделаем это снова для вас',
                    'icon' => 'flaticon-target',
                ]);
                echo ComponentItemWidget::widget([
                    'text' => 'Эксперта во всех современных сервисах и программах по бухгалтерии',
                    'icon' => 'flaticon-search',
                ]);
                echo ComponentItemWidget::widget([]);
                echo ComponentItemWidget::widget([
                    'text' => 'Вас обслуживает специалисты высшего класса',
                    'icon' => 'flaticon-employee-1',
                ]);
                echo ComponentItemWidget::widget([
                    'text' => 'Четкие регламенты и автоматизация позволяют делать работу быстрее и эффективнее',
                    'icon' => 'flaticon-document',
                ]);
                echo ComponentItemWidget::widget([]);
                echo ComponentItemWidget::widget([
                    'text' => 'Мы поддерживаем доступный уровень цен',
                    'icon' => 'flaticon-wallet',
                ]);
                ?>
            </div>
        </div>
        <div class="lower-content centred">
            <div class="sec-title style-three centred">
                <div class="btn-box btn registering-box-button">
                    <a href="blog-grid.html" class="theme-btn style-three" data-bs-toggle="modal" data-bs-target="#emailModal">
                        Начните работать со мной и экономьте до 30% на бухобслуживании </i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- intro-section end -->


<!-- funfact-style-two -->
<section class="funfact-style-two centred">
    <div class="pattern-layer" style="background-image: url(images/shape/shape-11.png);"></div>
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-lg-3 col-md-6 col-sm-12 counter-column">
                <div class="counter-block-two wow slideInUp" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="count-outer count-box">
                        <span class="count-text" data-speed="1500" data-stop="30">0</span>+
                    </div>
                    <h3>Довольных <br> клиентов</h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 counter-column">
                <div class="counter-block-two wow slideInUp" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="count-outer count-box">
                        <span class="count-text" data-speed="1500" data-stop="100">0</span>+
                    </div>
                    <h3>Налоговых <br />проверок</h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 counter-column">
                <div class="counter-block-two wow slideInUp" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="count-outer count-box">
                        <span class="count-text" data-speed="1500" data-stop="3">0</span>М+
                    </div>
                    <h3>Сэкономлено <br />на налогах</h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 counter-column">
                <div class="counter-block-two wow slideInUp" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="count-outer count-box">
                        <span class="count-text" data-speed="1500" data-stop="8">0</span>
                    </div>
                    <h3>Города <br />присутствия</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- funfact-style-two end -->


<!-- category section -->
<section class="project-style-two pricing-section" style="background-image: url(images/background/people.jpg);">
    <a name='clients'></a>
    <div class="auto-container">
        <div class="sec-title style-three centred">
            <h5></h5>
            <h2>Категории клиентов</h2>
        </div>
        <div class="project-carousel-2 owl-carousel owl-theme owl-nav-none">
            <?php echo CategoryWidget::widget([
                'name' => 'IT компании',
                'description' => 'Выбор оптимальной схемы налогообложения, регистрация как ИТ-компания',
                'text' => 'подробнее',
                'image' => 'images/clients/it.jpg',
            ]); ?>
            <?php echo CategoryWidget::widget([
                'name' => 'Предприятия оптовой и розничной торговли, маркетплейсы',
                'description' => 'Выбор оптимальной схемы налогообложения, оперативный бухучет, банк и касса',
                'text' => 'подробнее',
                'image' => 'images/clients/shop.jpg',
            ]); ?>
            <?php echo CategoryWidget::widget([
                'name' => 'Производство, грузоперевозки',
                'description' => 'Выбор оптимальной схемы налогообложения, оперативный бухучет, банк и касса',
                'text' => 'подробнее',
                'image' => 'images/clients/factory.jpg',
            ]); ?>
            <?php echo CategoryWidget::widget([
                'name' => 'Медицинские и ветеринарные услуги',
                'description' => 'Выбор оптимальной схемы налогообложения, банк и касса',
                'text' => 'подробнее',
                'image' => 'images/clients/medcine.jpg',
            ]); ?>
        </div>
    </div>
</section>
<!-- category section end -->


<!-- tarif-section -->
<section class="pricing-section">
    <a name='tarifs'></a>
    <div class="auto-container">
        <div class="tabs-box">
            <div class="upper-box">
                <div class="title-inner clearfix">
                    <div class="sec-title style-three left pull-left">
                        <!-- <h5>Тарифы</h2> -->
                        <h1>Бухгалтерское сопровождение</h1>
                    </div>
                    <div class="text pull-left">
                        <p>Услуги по ведению бухгалтерии и сдачи отчетности </p>
                    </div>
                </div>
            </div>
            <div class="tabs-content">
                <div class="tab active-tab" id="tab-1">
                    <div class="row clearfix">
                        <?php echo TarifItemWidget::widget([
                            'title' => 'Комплексное бухгалтерское сопровождение',
                            'isRecomended' => true,
                            'price_line1' => 'от 2500',
                            'price_line2' => 'руб/мес',
                            'description' => 'Базовая стоимость обслуживания включает в себя все необходимые процедуры для начала ведения бизнеса с небольшим оборотом и 1-м сотрудником.',
                            'button' => 'Заказать',
                        ]); ?>
                        <?php echo TarifItemWidget::widget([
                            'title' => 'Восстановление бухгалтерского учета',
                            'isRecomended' => false,
                            'price_line1' => 'от 3000',
                            'price_line2' => 'руб/мес',
                            'description' => 'Восстановлению могут подлежать как отдельные участки учета, так и определенные периоды и включает в себя комплексные меры, начиная от наведения порядка в бухгалтерской программе до поиска утраченных документов.',
                            'button' => 'Заказать',
                        ]); ?>
                        <?php echo TarifItemWidget::widget([
                            'title' => 'Консультирование',
                            'isRecomended' => false,
                            'price_line1' => 'от 2000',
                            'price_line2' => 'руб/мес',
                            'description' => 'Консультации могут оказываться как в устной, так и в письменной форме в виде разового или абонентского обслуживания по вопросам бухгалтерского, налогового учета, оптимизации и планирования налогообложения.',
                            'button' => 'Заказать',
                        ]); ?>
                        <?php echo TarifItemWidget::widget([
                            'title' => 'Кадровый учет',
                            'isRecomended' => false,
                            'price_line1' => 'от 500',
                            'price_line2' => 'руб/мес',
                            'description' => 'Кадровый учет включает в себя регистрацию, учёт и мониторинг движения сотрудников с оформлением всех необходимых документов, а также разработку Положений и актов кадрового делопроизводства.',
                            'button' => 'Заказать',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- tarif-section end -->


<!-- registering-section -->
<section class="pricing-section" style="background-image: url(images/background/stage.jpg);">
    <a name='services'></a>
    <div class="auto-container">
        <div class="tabs-box">
            <div class="upper-box">
                <div class="title-inner clearfix">
                    <div class="sec-title style-three left pull-left">
                        <!-- <h5>Тарифы</h2> -->
                        <h1>Регистрация бизнеса <br />ООО и ИП</h1>
                    </div>
                    <div class="text pull-left">
                        <p>Зарегистрируйте бизнес бесплатно!</p>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 insection-header">
                    <h2>Этапы регистрации:</h2>
                </div>
            </div>
            <div class="tabs-content">
                <div class="tab active-tab" id="tab-1">
                    <div class="row clearfix">
                        <?php echo TarifItemWidget::widget([
                            'title' => '1.Консультация.',
                            'description' => 'Подбор видов деятельности ОКВЭД. Выбор налогового режима. Сбор информации.',
                            'button' => false,
                        ]); ?>
                        <?php echo TarifItemWidget::widget([
                            'title' => '2. Подготовка документов',
                            'description' => 'Для создания ООО - Устав, Решение, Заявление Р11001, заявление на УСН (если нужно).<br /><br />
                                    Для открытия ИП - Заявление Р21001, заявление на упрощёнку (если нужно).',
                        ]); ?>
                        <?php echo TarifItemWidget::widget([
                            'title' => '3. Встреча в банке для открытия счета',
                            'description' => 'Вам необходимо подойти в банк для открытия счета',
                        ]); ?>
                        <?php echo TarifItemWidget::widget([
                            'title' => '4. Получение готовых документов',
                            'description' => 'Через 3 рабочих дня уведомление об успешной регистрации фирмы',
                            'button' => false,
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class='registering-box'>
                <div class="btn-box registering-box-button"><a href="blog-grid.html" class="theme-btn style-three">Открыть ООО</i></a></div>
                <div class="btn-box registering-box-button"><a href="blog-grid.html" class="theme-btn style-three">Открыть ИП</i></a></div>
            </div>
        </div>
    </div>
</section>
<!-- registering-section end -->


<!-- audit-section -->
<section class="pricing-section"    >
    <div class="auto-container">
        <div class="tabs-box">
            <div class="upper-box">
                <div class="title-inner clearfix">
                    <div class="sec-title style-three left pull-left">
                        <!-- <h5>Тарифы</h2> -->
                        <h1>Аудит бухгалтерского учета</h1>
                    </div>
                    <div class="text pull-left">
                        <p>Экспресс-аудит бесплатно!</p>
                    </div>
                </div>
            </div>
            <div class="tabs-content">
                <div class="tab active-tab" id="tab-1">
                    <div class="row clearfix">
                        <?php echo TarifItemWidget::widget([
                            'title' => 'Экспресс-аудит',
                            'isRecomended' => true,
                            'price_line1' => 'Бесплатно',
                            'price_line2' => '',
                            'description' => 'Если после экспресс-аудита вы решите остаться со мной на бухгалтерское обслуживание, экспресс-аудит останется вам в подарок вместо 10000 рублей!',
                            'button' => 'Заказать',
                        ]); ?>
                        <?php echo TarifItemWidget::widget([
                            'title' => 'Аудит 1С-Бухгалтерии',
                            'isRecomended' => false,
                            'price_line1' => 'от 5000',
                            'price_line2' => 'руб',
                            'description' => 'Проверю, верно ли отражаются операции в вашей программе 1С, правильно ли закрывается финансовый результат. <br /><br />Дам полезные рекомендации.',
                            'button' => 'Заказать',
                        ]); ?>
                        <?php echo TarifItemWidget::widget([
                            'title' => 'Инициативный аудит',
                            'isRecomended' => false,
                            'price_line1' => 'от 30000',
                            'price_line2' => 'руб',
                            'description' => 'Проверю полностью вашу бухгалтерию от учета основных средств до формирования финансового результата, правильно ли заполняются документы и рассчитываются налоги.
                                <br />Отчет предоставляю в письменном виде с ссылкой на нормативные документы.',
                            'button' => 'Заказать',
                        ]); ?>
                        <?php echo TarifItemWidget::widget([
                            'title' => 'Обязательный аудит',
                            'isRecomended' => false,
                            'price_line1' => 'от 50000',
                            'price_line2' => 'руб',
                            'description' => 'Проведу оценку бухгалтерского учета и финансовой отчетности на достоверность с выдачей аудиторского заключения и отчета о результатах проверки',
                            'button' => 'Заказать',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- audit-section end -->


<!-- steps-section -->
<section class="pricing-section" style="background-image: url(images/background/stage.jpg);">
    <div class="auto-container">
        <div class="tabs-box">
            <div class="upper-box">
                <div class="title-inner clearfix">
                    <div class="sec-title style-three left pull-left">
                        <!-- <h5>Тарифы</h2> -->
                        <h1>Этапы работ</h1>
                    </div>
                    <div class="text pull-left">
                        <p>Если у вас действующий бизнес и по какой-то причине вы ищите исполнителя по ведению бухгалтерских услуг, то мы с вами будем работать следующим образом:</p>
                    </div>
                </div>
            </div>
            <div class="tabs-content">
                <div class="tab active-tab" id="tab-1">
                    <div class="row clearfix">
                        <?php echo StageItemWidget::widget([
                            'title' => '1',
                            'image' => 'images/service/1.jpg',
                            'text' => 'Договариваемся о встрече в нашем офисе или в офисе Вашей компании',
                        ]); ?>
                        <?php echo StageItemWidget::widget([
                            'title' => '2',
                            'image' => 'images/service/2.jpg',
                            'text' => 'На встрече вы рассказываете о своей проблеме, показываете документы, которые у вас есть и которые вы можете предоставить',
                        ]); ?>
                        <?php echo StageItemWidget::widget([
                            'title' => '3',
                            'image' => 'images/service/3.jpg',
                            'text' => 'Я провожу экспресс-аудит за 10000 руб., который поможет определить реальное состояние бухгалтерского учета и определяем примерную стоимость бухгалтерского обслуживания в месяц',
                        ]); ?>
                        <?php echo StageItemWidget::widget([
                            'title' => '4',
                            'image' => 'images/service/4.jpg',
                            'text' => 'Вы решаете, остаетесь с нами или ищете другого исполнителя.',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- steps-section end -->


<!-- testimonial-section -->
<section class="testimonial-section alternet-2">
    <div class="auto-container">
        <div class="sec-title style-three centred">
            <h5></h5>
            <h2>Что говорят наши клиенты</h2>
        </div>
        <div class="testimonial-inner">
            <div class="client-testimonial-carousel owl-carousel owl-theme owl-nav-none owl-dots-none">
                <?php echo FeedbackTextWidget::widget([
                            'text' => 'Добрый день! Хотел бы поделиться отзывом о моём бухгалтере Венере Юматовой. С ней мы знакомы уже 4 года. В тот момент искал себе на предприятие грамотного бухгалтера с опытом работы, который может работать дистанционно, будет в курсе всех новых тенденций. Венера работает добросовестно, всегда довольно быстро решает все связанные с бухгалтерией вопросы, а я получаю большую свободу в других вопросах предприятия, т.к. бухгалтерия делается "под ключ".',
                        ]); ?>
                <?php echo FeedbackTextWidget::widget([
                            'text' => 'Я заказывал составление теста для оценки знаний кадрового сотрудника. Венера сделала отличный интересный тест, выявляющий знания и навыки в разных областях КДП и ТК РФ. Спасибо!',
                        ]); ?>
                <?php echo FeedbackTextWidget::widget([
                            'text' => 'Наша компания обращалась за помощью к Юматовой Венере при составлении бухгалтерской и налоговой отчетности, а также за консультационными услугами, на все заданные вопросы оперативно были предоставлены детальные ответы, что помогло нам в принятии решений. <br>
                            Юматова Венера является настоящим профессионалом, обладает высоким коэффициентом клиентоориентированности, обширными знаниями в области бухгалтерского учета, налогообложения, финансового анализа, своевременно решает поставленные задачи, очень добросовестно относится к своему делу <br>
                            За время работы с нашей компанией, Венера Юматова зарекомендовала себя, как надёжный и ответственный партнёр, выражаем благодарность за помощь в ведении бухгалтерского учета и надеемся на долгосрочные деловые отношения.',
                        ]); ?>
                <?php echo FeedbackTextWidget::widget([
                            'text' => 'Во время проверки помогла разобраться во многих вопросах бухгалтерского учета, в частности по вопросам учета тмц, членских взносов и многих других.
                            Провела консультацию по трудовому законодательству и дала много полезных советов по оформлению локальных нормативных документов фирмы.',
                        ]); ?>
                <?php echo FeedbackTextWidget::widget([
                            'text' => 'Венера Тагмановна навела порядок в бухгалтерской и налоговой отчетности. Восстановила аналитику по счетам, привела бухгалтерский учет в соответствие с законодательством. За недолгий срок своей работы прошла выездную налоговую проверку 2014-2015 годов хозяйственной деятельности предприятия, а также ревизионную проверку акционера. ОАО «ОКСК» признательна Юматовой В.Т. за квалифицированность и оперативность, грамотность и серьезное отношение к делу. Готовы рекомендовать Венеру Тагмановну как надежного и серьезного партнера.',
                        ]); ?>
                <?php echo FeedbackTextWidget::widget([
                            'text' => 'Решила обратиться, потому что мне Вас порекомендовал мой знакомый! Был очень доволен Вашей работой!
                            Критерий был знание и умение возврата НДС, умение работать с казахстанским заказчиком <br>
                            Я, если честно, тоже очень довольна! Вы ответственная и отзывчивая! Могу обратиться с любым вопросом и знаю, что получу подробный ответ!<br>
                            Я тоже начала немного разбираться в каких-то вопросах, но прям немного)! <br>
                            Что могу отметить: Это оперативность, надежность, качественное выполнение работы! Ещё мне нравится, что Вы всегда на связи! Что с любым вопросом можно обратиться Вы и проконсультируете и поможете выполнить!',
                        ]); ?>
                <?php echo FeedbackTextWidget::widget([
                            'text' => 'Она бухгалтер со стажем и хорошо разбирается в вопросах связанных с налогообложением и финансовым учетом. <br>
                            В ходе нашей беседы она рассказала какие выгоды и не выгоды ждут меня при оформлении как самозанятый, ИП или ООО. В итоге пока мы остановились на варианте самозанятого. Осталось только дождаться когда этот закон вступит в силу и на территории моей области. <br>
                            Благодарю за своевременную и такую нужную мне консультацию и вам ее советую. Она хорошо умеет разъяснять вещи связанные с бухгалтерией и поможет вам навести порядок.',
                        ]); ?>
                <?php echo FeedbackTextWidget::widget([
                            'text' => 'Были быстро получены грамотные ответы на все вопросы, проблема с налоговой решена',
                        ]); ?>
                <?php echo FeedbackTextWidget::widget([
                            'text' => 'Опытный бухгалтер! Советую!',
                        ]); ?>
                <?php echo FeedbackTextWidget::widget([
                            'text' => 'Хочется вам выразить благодарнось! За оказание помощи в решении непростых ситуаций в бухгалтерии, а так же за бесценный опыт',
                        ]); ?>
            </div>
            <div class="client-thumb-outer">
                <div class="client-thumbs-carousel owl-carousel owl-theme owl-nav-none owl-dots-none">
                    <?php echo FeedbackNameWidget::widget([
                            'name' => 'Сергей Аварницын',
                            'post' => 'директор',
                            'image' => false,
                        ]); ?>
                    <?php echo FeedbackNameWidget::widget([
                            'name' => 'Евгений Веселов',
                            'post' => 'HR',
                            'image' => false,
                        ]); ?>
                    <?php echo FeedbackNameWidget::widget([
                            'name' => 'Фоминых Г.Г.',
                            'post' => 'директор, ООО "ТД Золотой молочник"',
                            'image' => false,
                        ]); ?>
                    <?php echo FeedbackNameWidget::widget([
                            'name' => 'Ольга Калмыкова',
                            'post' => '',
                            'image' => false,
                        ]); ?>
                    <?php echo FeedbackNameWidget::widget([
                            'name' => 'Гашева Е.В.',
                            'post' => 'главный бухгалтер, ОАО "ОКСК"',
                            'image' => false,
                        ]); ?>
                    <?php echo FeedbackNameWidget::widget([
                            'name' => 'Наталья',
                            'post' => 'главный бухгалтер, ООО "Сибпромгруп"',
                            'image' => false,
                        ]); ?>
                    <?php echo FeedbackNameWidget::widget([
                            'name' => 'Юлия',
                            'post' => 'ИП',
                            'image' => false,
                        ]); ?>
                    <?php echo FeedbackNameWidget::widget([
                            'name' => 'Петр Феклистов',
                            'post' => 'ИП',
                            'image' => false,
                        ]); ?>
                    <?php echo FeedbackNameWidget::widget([
                            'name' => 'Денис Власов',
                            'post' => 'директор, ООО "Аудит-Информ"',
                            'image' => false,
                        ]); ?>
                    <?php echo FeedbackNameWidget::widget([
                            'name' => 'Татьяна',
                            'post' => 'ИП',
                            'image' => false,
                        ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- testimonial-section end -->


<?php
    echo EmailFormWidget::widget([]);
?>
