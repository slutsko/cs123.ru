<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;

AppAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon.ico')]);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- Fav Icon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Arimo:400,400i,700,700i&display=swap" rel="stylesheet">
    
    <!-- 2Gis -->
    <script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>

    <script type="text/javascript">
    var map;

    DG.then(function () {
        map = DG.map('map', {
            center: [54.985757, 73.379710],
            zoom: 17    
        });
        DG.marker([54.985757, 73.379710])
            .addTo(map)
            .bindPopup('<b>Бухэксперт   </b><br>г. Омск, ул. Щербанева, д. 25, оф. 404.3 <br> Email <a href="mailto:office@cs123.ru">office@cs123.ru</a><br><a href="tel:+79620353468">+7 (962) 035-34-68</a>');
    });
</script>
</head>

<!-- page wrapper -->
<body class="boxed_wrapper ltr">
    <?php $this->beginBody() ?>
    <!-- Preloader -->
    <div class="loader-wrap">
        <div class="preloader style-three"><div class="preloader-close">Preloader Close</div></div>
        <div class="layer layer-one"><span class="overlay"></span></div>
        <div class="layer layer-two"><span class="overlay"></span></div>        
        <div class="layer layer-three"><span class="overlay"></span></div>        
    </div>

    <!-- search-popup -->
    <div id="search-popup" class="search-popup">
        <div class="close-search"><span>Close</span></div>
        <div class="popup-inner">
            <div class="overlay-layer"></div>
            <div class="search-form">
                <form method="post" action="index.html">
                    <div class="form-group">
                        <fieldset>
                            <input type="search" class="form-control" name="search-input" value="" placeholder="Search Here" required >
                            <input type="submit" value="Search Now!" class="theme-btn style-four">
                        </fieldset>
                    </div>
                </form>
                <h3>Recent Search Keywords</h3>
                <ul class="recent-searches">
                    <li><a href="index.html">Finance</a></li>
                    <li><a href="index.html">Idea</a></li>
                    <li><a href="index.html">Service</a></li>
                    <li><a href="index.html">Growth</a></li>
                    <li><a href="index.html">Plan</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- search-popup end -->

    <!-- main header -->
    <header class="main-header style-one style-three">
        <div class="header-top">
            <div class="auto-container">
                <div class="top-inner clearfix">
                    <ul class="info top-left pull-left">
                        <li><i class="fas fa-map-marker-alt"></i>г. Омск, ул. Щербанева, д. 25, оф. 404.3</li>
                    </ul>
                    <div class="top-right pull-right">
                        <ul class="social-links clearfix">
                            <li><a href="https://www.facebook.com/venera.yumatova"><i class="fab fa-facebook-f"></i></a></li>
                            <!-- <li><a href="index.html"><i class="fab fa-google-plus-g"></i></a></li> -->
                            <!-- <li><a href="index.html"><i class="fab fa-twitter"></i></a></li> -->
                            <!-- <li><a href="index.html"><i class="fab fa-linkedin-in"></i></a></li> -->
                            <li><a href="https://www.instagram.com/buhgalter.konsultant/"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="https://vk.ru/buhgalter.konsultant"><i class="fab fa-vk"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-lower">
            <div class="outer-box">
                <div class="auto-container">
                    <div class="main-box clearfix">
                        <div class="logo-box pull-left">
                            <figure class="logo"><a href="index.html"><img src="images/logo.png" alt=""><p>Бухэксперт</p></a></figure>
                        </div>
                        <div class="menu-area pull-right clearfix">
                            <!--Mobile Navigation Toggler-->
                            <div class="mobile-nav-toggler">
                                <i class="icon-bar"></i>
                                <i class="icon-bar"></i>
                                <i class="icon-bar"></i>
                            </div>
                            <nav class="main-menu navbar-expand-md navbar-light">
                                <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                    <ul class="navigation clearfix">
                                        <li><a href="#start">Начало</a></li>
                                        <!-- <li><a href="#about">О нас</a></li> -->
                                        <li><a href="#clients">Клиенты</a></li>
                                        <li><a href="#tarifs">Тарифы</a></li>
                                        <li><a href="#services">Услуги</a></li>
                                        <li><a href="#contact">Контакты</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <div class="menu-right-content clearfix">
                                <div class="support-box">
                                    <i class="flaticon-smartphone-1"></i>
                                    <p>Позвоните нам прямо сейчас</p>
                                    <h3><a href="tel:+79620353468"> +7 (962) 035-34-68</a></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--sticky Header-->
        <div class="sticky-header">
            <div class="auto-container">
                <div class="outer-box clearfix">
                    <div class="logo-box pull-left">
                        <figure class="logo"><a href="index.html"><img src="images/logo.png" alt=""></a><p>Бухэксперт</p></figure>
                    </div>
                    <div class="menu-area pull-right">
                        <nav class="main-menu clearfix">
                            <!--Keep This Empty / Menu will come through Javascript-->
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- main-header end -->

    <!-- Mobile Menu  -->
    <div class="mobile-menu">
        <div class="menu-backdrop"></div>
        <div class="close-btn"><i class="fas fa-times"></i></div>
        
        <nav class="menu-box">
            <div class="nav-logo"><a href="index.html"><img src="images/logo.png" alt="" title=""></a></div>
            <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            <div class="contact-info">
                <h4>Контакты</h4>
                <ul>
                    <li><i class="fas fa-map-marker-alt"></i> г. Омск, ул. Щербанева, д. 25, оф. 404.3 </li>
                    <li><i class="fas fa-envelope"></i> Email <a href="mailto:office@cs123.ru">office@cs123.ru</a></li>
                    <li><i class="fas fa-headphones"></i><a href="tel:+79620353468"> +7 (962)  035-34-68</a></li>
                </ul>
            </div>
            <div class="social-links">
                <ul class="clearfix">
                    <!-- <li><a href="index.html"><span class="fab fa-twitter"></span></a></li> -->
                    <li><a href="https://www.facebook.com/venera.yumatova"><span class="fab fa-facebook-square"></span></a></li>
                    <!-- <li><a href="index.html"><span class="fab fa-pinterest-p"></span></a></li> -->
                    <li><a href="https://www.instagram.com/buhgalter.konsultant/"><span class="fab fa-instagram"></span></a></li>
                    <li><a href="https://vk.ru/buhgalter.konsultant"><i class="fab fa-vk"></i></a></li>
                    <!-- <li><a href="index.html"><span class="fab fa-youtube"></span></a></li> -->
                </ul>
            </div>
        </nav>
    </div><!-- End Mobile Menu -->

    <?= $content ?>

    <!-- main-footer -->
    <footer class="main-footer alternet-3">
        <a name='contact'></a>
        <div class="footer-top">
            <div class="auto-container">
                <div class="widget-section">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-5 col-sm-12 footer-column">
                            <div class="footer-widget logo-widget">
                                <figure class="footer-logo"><a href="index.html"><img src="images/logo.png" alt=""></a></figure>
                                <div class="text">
                                    <p>Когда в ежедневной работе нужен кто-то больше, чем просто бухгалтер</p>
                                </div>
                                <ul class="info-list clearfix">
                                    <li><i class="fas fa-map-marker-alt"></i>г. Омск, ул. Щербанева, д. 25, оф. 404.3 </li>
                                    <li><i class="fas fa-envelope"></i>Email <a href="mailto:office@cs123.ru">office@cs123.ru</a></li>
                                    <li><i class="fas fa-headphones"></i><a href="tel:+79620353468">+7 (962)  035-34-68</a></li>
                                </ul>
                                <ul class="social-links clearfix">
                                    <!-- <li><a href="index.html"><i class="fab fa-twitter"></i></a></li> -->
                                    <li><a href="https://www.facebook.com/venera.yumatova"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="https://www.instagram.com/buhgalter.konsultant/"><i class="fab fa-instagram"></i></a></li>
                                    <li><a href="https://vk.ru/buhgalter.konsultant"><i class="fab fa-vk"></i></a></li>
                                    <!-- <li><a href="index.html"><i class="fab fa-linkedin-in"></i></a></li> -->
                                    <!-- <li><a href="index.html"><i class="fab fa-pinterest-p"></i></a></li> -->
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-1 footer-column"></div>
                        <div class="col-lg-7 col-md-6 col-sm-12 footer-column">
                            <div id="map" style="width:600px; height:280px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright"><p>&copy; <?php echo date("Y"); ?> <a href="index.html">Бухэксперт </a> - Все права защищены.</p></div>
            </div>
        </div>
    </footer>
    <!-- main-footer end -->

    <!--Scroll to top-->
    <button class="scroll-top style-three scroll-to-target" data-target="html">
        <span class="fa fa-arrow-up"></span>
    </button>

    <!-- END sidebar widget item -->
    <?php $this->endBody() ?>
</body><!-- End of .page_wrapper -->
</html>
<?php $this->endPage() ?>
