<div class="col-lg-3 col-md-6 col-sm-12 pricing-block">
    <div class="pricing-block-one">
        <div class="pricing-table">
            <div class="table-header">
                <h3><?= $title ?></h3>
                <?php if ($price_line1) { ?>
                    <div class="price-box">
                        <?= $isRecomended ? '<span>рекомендуем</span>' : false ?>
                        <h2><?= $price_line1 ?></h2>
                        <h2><?= $price_line2 ? $price_line2 : '&nbsp;' ?></h2>
                    </div>
                <?php } ?>
            </div>
            <div class="table-content">
                <ul class="clearfix"> 
                <?= $description ?> 
                </ul>
            </div>
            <div class="table-footer">
                <?php if ($button) { ?>
                <a href="index-3.html"><?= $button ?></a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
