<div class="slide-item">
    <div class="image-layer" style="background-image:url(<?= $image ?>)"></div>
    <div class="auto-container">
        <div class="content-box centred">
            <h1><?= $title ?></h1>
            <p><?= $text ?></p>
            <div class="btn-box">
                <a href="<?= $url ?>" class="theme-btn style-three"><?= $urlTitle ?></a>
                <!-- <a href="index" class="banner-btn">Learn More</a> -->
            </div>
        </div>
    </div>
</div>
