<!-- Modal -->
<div class="modal fade" id="emailModal" tabindex="-1" aria-labelledby="emailModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="emailModalLabel">Сосредоточтесь на бизнесе - бухгалтерию сделаем мы</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="form-inner">
          <form action="contact.html" method="post">
            <div class="form-floating mb-3">
              <input type="email" class="form-control" id="floatingInput2" placeholder="Иванов Иван Иванович">
              <label for="floatingInput2">Как к вам обращаться</label>
            </div>

            <div class="form-floating mb-3">
              <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com">
              <label for="floatingInput">Email</label>
            </div>

            <div class="form-floating mb-3">
              <input type="email" class="form-control" id="floatingInput3" placeholder="8-987-654-4321">
              <label for="floatingInput3">Телефон</label>
            </div>

            <div class="form-floating mb-3">
              <select class="form-select" id="floatingSelect" aria-label="Default select example">
                <option selected>Выберите вариант</option>
                <option value="Открыть ООО">Открыть ООО</option>
                <option value="Открыть ИП">Открыть ИП</option>
                <option value="Аудит бухгалтерского учета">Аудит бухгалтерского учета</option>
                <option value="Комплексное бухгалтерское сопровождение">Комплексное бухгалтерское сопровождение</option>
                <option value="Восстановление бухгалтерского учета">Восстановление бухгалтерского учета</option>
                <option value="Консультирование">Консультирование</option>
                <option value="Кадровый учет">Кадровый учет</option>
              </select>
              <label for="floatingSelect">Тип запроса</label>
            </div>

            <div class="form-floating">
              <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea" style="height: 100px"></textarea>
              <label for="floatingTextarea">Дополнительно</label>
            </div>

          </form>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Отмена</button> -->
        <button type="button" class="btn theme-btn style-three">Сделать запрос</button>
      </div>
    </div>
  </div>
</div>