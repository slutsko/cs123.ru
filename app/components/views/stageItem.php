<div class="col-lg-3 col-md-6 col-sm-12 pricing-block">
    <div class="pricing-block-one">
        <div class="pricing-table">
            <figure class="image-box">
                <div class="overlay-box-1"></div>
                <div class="overlay-box-2"></div>
                <img src="<?= $image ?>" alt="">
            </figure>
            <div class="lower-content">
                <h3><?= $title ?></h3>
                <p><?= $text ?></p>
            </div>
        </div>
    </div>
</div>