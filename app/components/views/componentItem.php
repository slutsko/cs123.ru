<?php if ($text) { ?>
    <div class="col-lg-4 col-md-6 col-sm-12 single-column">
        <div class="single-item">
            <div class="inner-box">
                <div class="icon-box"><i class="<?= $icon ?>"></i></div>
                <!-- <h3><a href="index-3.html">Business Advantages</a></h3> -->
                <p><?= $text ?></p>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="col-lg-4 d-none d-lg-block">
        <div class="single-item">
            <img class='main-photo' src="<?= $image ?>" alt="">
        </div>
    </div>
<?php } ?>