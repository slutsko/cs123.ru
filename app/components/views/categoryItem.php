<div class="project-inner">
    <div class="inner-box">
        <figure class="image-box"><img src="<?= $image ?>" alt=""></figure>
        <div class="content-box">
            <span><?= $name ?></span>
            <h3><?= $description ?></h3>
            <p><?= $text ?></p>
            <!-- <a href="<?= $url ?>"><i class="fas fa-arrow-right"></i><span>Подробнее</span></a> -->
        </div>
    </div>
</div>
