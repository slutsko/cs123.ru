<div class="testimonial-block">
    <div class="text">
        <i class="fas fa-quote-right"></i>
        <p><?= $text ?></p>
    </div>
</div>
