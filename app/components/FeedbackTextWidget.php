<?php

namespace app\components;
use yii\base\Widget;

class FeedbackTextWidget extends Widget {
    
    public $text;

    public function init() {
        parent::init();

        $this->text = $this->text ? $this->text : '';

    }

    public function run($config = [])
    {
        return $this->render('feedbackTextItem', [
            'text' => $this->text,
        ]);
    }

}