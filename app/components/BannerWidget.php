<?php

namespace app\components;
use yii\base\Widget;

class BannerWidget extends Widget {
    
    public $url;
    public $urlTitle;
    public $title;
    public $text;
    public $image;

    public function init() {
        parent::init();

        $this->url = $this->url ? $this->url : '';
        $this->urlTitle = $this->urlTitle ? $this->urlTitle : '';
        $this->title = $this->title ? $this->title : '';
        $this->text = $this->text ? $this->text : '';
        $this->image = $this->image ? $this->image : '';

    }

    public function run($config = [])
    {
        return $this->render('bannerItem', [
            'url' => $this->url,
            'urlTitle' => $this->urlTitle,
            'title' => $this->title,
            'text' => $this->text,
            'image' => $this->image,
        ]);
    }

}