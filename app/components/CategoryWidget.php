<?php

namespace app\components;
use yii\base\Widget;

class CategoryWidget extends Widget {
    
    public $url;
    public $name;
    public $description;
    public $text;
    public $image;

    public function init() {
        parent::init();

        $this->url = $this->url ? $this->url : '';
        $this->name = $this->name ? $this->name : '';
        $this->description = $this->description ? $this->description : '';
        $this->text = $this->text ? $this->text : '';
        $this->image = $this->image ? $this->image : '';

    }

    public function run($config = [])
    {
        return $this->render('categoryItem', [
            'url' => $this->url,
            'name' => $this->name,
            'description' => $this->description,
            'text' => $this->text,
            'image' => $this->image,
        ]);
    }

}