<?php

namespace app\components;
use yii\base\Widget;

class TarifItemWidget extends Widget {
    
    public $title;
    public $isRecomended;
    public $price_line1;
    public $price_line2;
    public $description;
    public $button;

    public function init() {
        parent::init();

        $this->title = $this->title ? $this->title : '';
        $this->isRecomended = $this->isRecomended ? $this->isRecomended : false;
        $this->price_line1 = $this->price_line1 ? $this->price_line1 : false;
        $this->price_line2 = $this->price_line2 ? $this->price_line2 : false;
        $this->description = $this->description ? $this->description : '';
        $this->button = $this->button ? $this->button : false;

    }

    public function run($config = [])
    {
        return $this->render('tarifItem', [
            'title' => $this->title,
            'isRecomended' => $this->isRecomended,
            'price_line1' => $this->price_line1,
            'price_line2' => $this->price_line2,
            'description' => $this->description,
            'button' => $this->button,
        ]);
    }

}