<?php

namespace app\components;
use yii\base\Widget;

class ComponentItemWidget extends Widget {
    
    public $text;
    public $icon;
    public $image;

    public function init() {
        parent::init();

        $this->text = $this->text ? $this->text : false;
        $this->icon = $this->icon ? $this->icon : false;
        $this->image = $this->image ? $this->image : false;

    }

    public function run($config = [])
    {
        return $this->render('componentItem', [
            'text' => $this->text,
            'icon' => $this->icon,
            'image' => $this->image,
        ]);
    }

}