<?php

namespace app\components;
use yii\base\Widget;

class StageItemWidget extends Widget {
    
    public $image;
    public $title;
    public $text;
    public $link;

    public function init() {
        parent::init();

        $this->image = $this->image ? $this->image : false;
        $this->title = $this->title ? $this->title : false;
        $this->text = $this->text ? $this->text : false;
        $this->link = $this->link ? $this->link : false;

    }

    public function run($config = [])
    {
        return $this->render('stageItem', [
            'image' => $this->image,
            'title' => $this->title,
            'text' => $this->text,
            'link' => $this->link,
        ]);
    }

}