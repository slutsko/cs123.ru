<?php

namespace app\components;
use yii\base\Widget;

class EmailFormWidget extends Widget {
    
    public function init() {
        parent::init();
    }

    public function run($config = [])
    {
        return $this->render('emailForm', []);
    }

}