<?php

namespace app\components;
use yii\base\Widget;

class FeedbackNameWidget extends Widget {
    
    public $post;
    public $name;
    public $image;

    public function init() {
        parent::init();

        $this->post = $this->post ? $this->post : '';
        $this->name = $this->name ? $this->name : '';
        $this->image = $this->image ? $this->image : '';

    }

    public function run($config = [])
    {
        return $this->render('feedbackNameItem', [
            'name' => $this->name,
            'post' => $this->post,
            'image' => $this->image,
        ]);
    }

}